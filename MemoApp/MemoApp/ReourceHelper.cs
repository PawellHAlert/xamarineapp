﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MemoApp
{
    public static  class ResourceHelper
    {
        public static HtmlWebViewSource LoadHtml(string resorceName)
        {
            var assembly = typeof(ResourceHelper).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream(resorceName);
            string html = string.Empty;
            using (var reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd();
            }

           return  new HtmlWebViewSource() { Html = html };
        }
    }
}
