﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MemoApp
{
    public partial class MainPage : ContentPage
    {
        private Image[] images = new Image[4];
        public MainPage()
        {
            InitializeComponent();
            images[0] = image0;
            images[1] = image1;
            images[2] = image2;
            images[3] = image3;
        }

        void OnTapGestureRecognizerTapped(object sender, EventArgs args)
        {
            foreach (Image i in images)
            {
                if(i.Id == ((Image)sender).Id)
                {
                    i.Source = "dayAdvicePressed.jpg";
                }
                else
                {
                    i.Source = "dayAdvice.jpg";
                }
            }

            if (((Image)sender).Equals(image0))
            {


                webView1.Source = ResourceHelper.LoadHtml("MemoApp.Resources.sites.one.testPage1.html");
            }
            if (((Image)sender).Equals(image1))
            {
                webView1.Source = ResourceHelper.LoadHtml("MemoApp.Resources.sites.two.testPage2.html");
            }
            if (((Image)sender).Equals(image2))
            {
                webView1.Source = ResourceHelper.LoadHtml("MemoApp.Resources.sites.three.testPage3.html");
            }
            if (((Image)sender).Equals(image3))
            {
                webView1.Source = ResourceHelper.LoadHtml("MemoApp.Resources.sites.four.testPage4.html");
            }

        }
    }
}
